# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 12:35:59 2021

@author: 40004808
"""

import cv2
import numpy as np
import yaml

cv2.namedWindow('My Window', cv2.WINDOW_KEEPRATIO)
cv2.setWindowProperty('My Window', cv2.WND_PROP_ASPECT_RATIO, cv2.WINDOW_KEEPRATIO)
# cv2.setWindowProperty('My Window',cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

with open('links.yaml') as file:
    try:
        urls = None
        data = yaml.safe_load(file)
        for key, value in data.items():
            #print(key, ":", value)
            urls = value

        cap1 = cv2.VideoCapture(urls[0])
        cap2 = cv2.VideoCapture(urls[1])
        cap3 = cv2.VideoCapture(urls[2])
        cap4 = cv2.VideoCapture(urls[3])

        while (cap1.isOpened() or cap2.isOpened() or cap3.isOpened() or cap4.isOpened()) :
            frame1 = cap1.read()[1]
            frame2 = cap2.read()[1]
            frame3 = cap3.read()[1]
            frame4 = cap4.read()[1]
            Hori1 = np.concatenate((frame1, frame2), axis=1)
            Hori2 = np.concatenate((frame3, frame4), axis=1)

            Verti = np.concatenate((Hori1, Hori2), axis=0)

            cv2.imshow('My Window', Verti)
            if (cv2.waitKey(20) & 0xFF == ord('q')) or (cv2.getWindowProperty('My Window', 0)):
                break
        cap1.release()
        cap2.release()
        cap3.release()
        cap4.release()
        cv2.destroyAllWindows()
        exit()

    except yaml.YAMLError as exception:
        print(exception)
